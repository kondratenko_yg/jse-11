package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.service.ProjectService;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.service.ProjectTaskService;

import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    public ProjectController(ProjectService projectService, ProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
        final String description = scanner.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT INDEX:]");
        final Project project = projectService.findByIndex(inputIndexCheckFormat());
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PROJECT NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
            final String description = scanner.nextLine();
            projectService.update(project.getId(), name, description);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final Project project = projectService.findById(inputIdCheckFormat());
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PROJECT NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
            final String description = scanner.nextLine();
            projectService.update(project.getId(), name, description);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        projectTaskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex() {
        System.out.println("[CLEAR PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = projectService.removeByIndex(inputIndexCheckFormat());
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[CLEAR PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeProjectById() {
        System.out.println("[CLEAR PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID: ");
        final Project project = projectService.removeById(inputIdCheckFormat());
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
            System.out.println("[OK]");
        }
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    public int viewProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = projectService.findByIndex(inputIndexCheckFormat());
        viewProject(project);
        return 0;
    }

    public int viewProjectById() {
        System.out.println("ENTER PROJECT ID: ");
        final Project project = projectService.findById(inputIdCheckFormat());
        viewProject(project);
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        for (final Project project : projectService.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        if (index > 1) System.out.println("[OK]");
        else System.out.println("[PROJECTS ARE NOT FOUND]");
        return 0;
    }

}
