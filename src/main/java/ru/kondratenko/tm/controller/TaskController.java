package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.TaskService;
import ru.kondratenko.tm.entity.Task;

import java.util.List;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
        final String description = scanner.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK INDEX:]");
        final Task task = taskService.findByIndex(inputIndexCheckFormat());
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            taskService.update(task.getId(), name, description);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK ID:]");
        final Task task = taskService.findById(inputIdCheckFormat());
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            taskService.update(task.getId(), name, description);
            System.out.println("[OK]");
            return 0;
        }
    }

    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        projectTaskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName() {
        System.out.println("[CLEAR TASK BY NAME]");
        System.out.println("ENTER TASK NAME: ");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[CLEAR TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX: ");
        final Task task = taskService.removeByIndex(inputIndexCheckFormat());
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() {
        System.out.println("[CLEAR TASK BY ID]");
        System.out.println("ENTER TASK ID: ");
        final Task task = taskService.removeById(inputIdCheckFormat());
        if (task == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("ENTER TASK INDEX: ");
        final Task task = taskService.findByIndex(inputIndexCheckFormat());
        viewTask(task);
        return 0;
    }

    public int viewTaskById() {
        System.out.println("ENTER TASK ID: ");
        final Task task = taskService.findById(inputIdCheckFormat());
        viewTask(task);
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("[TASKS ARE NOT FOUND]");
            return;
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    public int listTask() {
        System.out.println("[LIST TASK]");
        viewTasks(taskService.findAll());
        return 0;
    }

    public int listTaskByProjectId() {
        System.out.println("[LIST TASKS BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID: ");
        final long projectId = inputIdCheckFormat();
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        viewTasks(tasks);
        return 0;
    }

    public int addTaskToProjectByIds() {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("ENTER PROJECT ID: ");
        final long projectId = inputIdCheckFormat();
        System.out.println("ENTER TASK ID: ");
        final long taskId = inputIdCheckFormat();
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromProjectByIds() {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("ENTER PROJECT ID: ");
        final long projectId = inputIdCheckFormat();
        System.out.println("ENTER TASK ID: ");
        final long taskId = inputIdCheckFormat();
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

}
