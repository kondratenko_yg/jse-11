package ru.kondratenko.tm.service;

import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name) {
        if (name == null) return null;
        return taskRepository.create(name);
    }

    public Task create(final String name, final String description) {
        if (name == null) return null;
        if (description == null) return null;
        return taskRepository.create(name, description);
    }

    public Task update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null) return null;
        if (description == null) return null;
        return taskRepository.update(id, name, description);
    }

    public Task findByIndex(final int index) {
        return taskRepository.findByIndex(index);
    }

    public Task findByName(final String name) {
        if (name == null) return null;
        return taskRepository.findByName(name);
    }

    public Task findById(final Long id) {
        if (id == null) return null;
        return taskRepository.findById(id);
    }

    public Task removeByIndex(final int index) {
        return taskRepository.removeByIndex(index);
    }

    public Task removeById(final Long id) {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    public Task removeByName(final String name) {
        if (name == null) return null;
        return taskRepository.removeByName(name);
    }

    public void clear() {
        taskRepository.clear();
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (projectId == null) return null;
        if (id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

}
