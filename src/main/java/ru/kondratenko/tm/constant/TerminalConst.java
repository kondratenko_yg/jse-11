package ru.kondratenko.tm.constant;

public class TerminalConst {
    public static final String ABOUT = "about";
    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String EXIT = "exit";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    public static final String PROJECT_VIEW_BY_ID = "project-view-by-id";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    public static final String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW_BY_INDEX = "task-view-by-index";
    public static final String TASK_VIEW_BY_ID = "task-view-by-id";
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String TASK_LIST_BY_PROJECT_ID = "task-list-by-project-id";
    public static final String TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-task-by-ids";
    public static final String TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-from-project-by-ids";

    public static final String REGISTRY = "registry";
    public static final String LOG_OFF = "log-off";

    public static final String USER_CREATE = "user-create";
    public static final String USER_CLEAR = "user-clear";
    public static final String USER_LIST = "user-list";
    public static final String USER_VIEW_BY_LOGIN = "user-view-by-login";
    public static final String USER_REMOVE_BY_LOGIN = "user-remove-by-login";
    public static final String USER_UPDATE_BY_LOGIN = "user-update-by-login";

    public static final String[] ROLE_USER = new String[]{
            REGISTRY, LOG_OFF,
            USER_CREATE,
            USER_UPDATE_BY_LOGIN,
            ABOUT, HELP, VERSION, EXIT,
            PROJECT_CREATE, TASK_CREATE,
            PROJECT_CLEAR, TASK_CLEAR,
            PROJECT_LIST, TASK_LIST,
            PROJECT_VIEW_BY_INDEX, TASK_VIEW_BY_INDEX,
            PROJECT_VIEW_BY_ID, TASK_VIEW_BY_ID,
            PROJECT_REMOVE_BY_INDEX, TASK_REMOVE_BY_INDEX,
            PROJECT_REMOVE_BY_ID, TASK_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_NAME, TASK_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_INDEX, TASK_UPDATE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, TASK_UPDATE_BY_ID,
            TASK_LIST_BY_PROJECT_ID,
            TASK_ADD_TO_PROJECT_BY_IDS,
            TASK_REMOVE_FROM_PROJECT_BY_IDS};

    public static final String[] ROLE_ADMIN = new String[]{
            REGISTRY, LOG_OFF,
            USER_CREATE, USER_CLEAR, USER_LIST, USER_VIEW_BY_LOGIN, USER_REMOVE_BY_LOGIN, USER_UPDATE_BY_LOGIN,
            ABOUT, HELP, VERSION, EXIT,
            PROJECT_CREATE, TASK_CREATE,
            PROJECT_CLEAR, TASK_CLEAR,
            PROJECT_LIST, TASK_LIST,
            PROJECT_VIEW_BY_INDEX, TASK_VIEW_BY_INDEX,
            PROJECT_VIEW_BY_ID, TASK_VIEW_BY_ID,
            PROJECT_REMOVE_BY_INDEX, TASK_REMOVE_BY_INDEX,
            PROJECT_REMOVE_BY_ID, TASK_REMOVE_BY_ID,
            PROJECT_REMOVE_BY_NAME, TASK_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_INDEX, TASK_UPDATE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, TASK_UPDATE_BY_ID,
            TASK_LIST_BY_PROJECT_ID,
            TASK_ADD_TO_PROJECT_BY_IDS,
            TASK_REMOVE_FROM_PROJECT_BY_IDS};

    public static final String[] ROLE_ALL = new String[]{
            REGISTRY, LOG_OFF,
            ABOUT, HELP, VERSION, EXIT
    };

}
