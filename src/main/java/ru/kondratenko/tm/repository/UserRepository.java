package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private List<User> users = new ArrayList<>();

    public User create(final String login, final String password, final String firstName,
                       final String lastName, final String[] role) {
        User user = findByLogin(login);
        if (user == null) {
            user = new User(login, password, firstName, lastName, role);
            users.add(user);
        }
        return user;
    }

    public User findByLogin(final String login) {
        for (final User user : users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User update(final String login, final String password, final String firstName, final String lastName) {
        User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public User removeByName(final String login) {
        User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public void clear() {
        users.clear();
    }

    public List<User> findAll() {
        return users;
    }

}
